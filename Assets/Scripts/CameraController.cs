﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{
    private Vector3 cameraPosition;
    private bool cameraLocked = true;

    // Test values in order: 30, 10, 8, 20, 50
    public float zoomSpeed, moveSpeed, minZoom, maxZoom, mouseDetectionBorder;
    public GameObject player;

    private void Update()
    {
        // Toggle camera lock
        if (Input.GetKeyDown(KeyCode.Y))
        {
            cameraLocked = !cameraLocked;
        }

        if (cameraLocked)
        {
            // Lock the camera to the player, but keep the y position
            transform.position = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z - 6.0f);
        }
        else
        {
            // Get the current camera position
            cameraPosition = transform.position;

            // Zoom in
            if (Input.GetAxis("Mouse ScrollWheel") > 0f && transform.position.y > minZoom)
            {
                cameraPosition.y -= zoomSpeed * Time.deltaTime;
            }
            // Zoom out
            if (Input.GetAxis("Mouse ScrollWheel") < 0f && transform.position.y < maxZoom)
            {
                cameraPosition.y += zoomSpeed * Time.deltaTime;
            }

            // Move camera using mouse
            if (Input.mousePosition.y >= Screen.height - mouseDetectionBorder)
            {
                cameraPosition.z += moveSpeed * Time.deltaTime;
            }
            if (Input.mousePosition.y <= mouseDetectionBorder)
            {
                cameraPosition.z -= moveSpeed * Time.deltaTime;
            }
            if (Input.mousePosition.x >= Screen.width - mouseDetectionBorder)
            {
                cameraPosition.x += moveSpeed * Time.deltaTime;
            }
            if (Input.mousePosition.x <= mouseDetectionBorder)
            {
                cameraPosition.x -= moveSpeed * Time.deltaTime;
            }

            // Add the movement on the camera
            transform.position = cameraPosition;
        }
    }
}
