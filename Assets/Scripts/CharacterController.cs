﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Require Rigidbody and CapsuleCollider so they won't be forgotten and it also makes the script plug-and-play
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class CharacterController : MonoBehaviour {
    private Rigidbody rb;
    private bool isGrounded = false;

    // Define the speed player moves and jumps
    public float movementSpeed = 10;
    public float jumpForce = 5;

    private void Start()
    {
        // Assign rb with the component 
        rb = GetComponent<Rigidbody>();
    }

    // Use FixedUpdate instead of a regular update because of the rigidbody which uses physics engine
    private void FixedUpdate()
    {
        // Player movement
        PlayerMovement();
    }

    // Control the basic player movement e.g. moving around and jumping using the physics system
    void PlayerMovement()
    {
        // Get the horizontal and vertical axes without smoothing to make movement more accurate
        float horizontalMovement = Input.GetAxisRaw("Horizontal");
        float verticalMovement = Input.GetAxisRaw("Vertical");

        // Make the movement vector with the axes normalized to prevent going faster when moving diagonally
        // and multiply it by the speed and fixed delta time which could also be normal delta time
        // since it is already called in FixedUpdate
        Vector3 movement = new Vector3(horizontalMovement, 0, verticalMovement).normalized * movementSpeed * Time.fixedDeltaTime;
        
        // Add the movement vector to the player's rigidbody's position and let the rigidbody do the movement
        // Do noth bother transform directly because it might not be in sync with the physics
        rb.MovePosition(rb.position + movement);

        // Jump if on ground using space key, could also use "jump" key assigned in the editor input settings
        // Jump is quite airy if not adding more code to fasten the fall after velocity.y has reached the peak, aka velocity.y == 0 && !grounded
        if (Input.GetKey(KeyCode.Space) && isGrounded)
        {
            // Using impulse to make jump more elastic
            rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
        }
    }

    // Check if the player is grounded using physics collision detection
    private void OnCollisionEnter(Collision collision)
    {
        isGrounded = true;
    }

    // Check if the player is not grounded using physics collision detection
    private void OnCollisionExit(Collision collision)
    {
        isGrounded = false;
    }
}
