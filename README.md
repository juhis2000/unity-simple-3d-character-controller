This project was created using Unity 2018.1.  
After making a few projects in C++ without an engine I wanted to get back to
developing games with Unity since it is widely used Engine in the industry.  
This project is fairly simpple third person character controller using
rigidbody and Unity's physics system overall. There are two script that can
be found in Assets/Scripts directory. One controls the player movement and the
other controls the camera movement. The scripts are plug-and-play.  
Player movement:  
Player is moved by first getting the axes and the adding it to rigidbody's 
position multiplied by all the neccessary stuff that can be seen in the script.
Player moves using wasd, arrow keys, or controller and jumps using space key.  
Camera movement:  
Camera moves to 8 directions when mouse gets near enough the screen border and
is locked with Y key by default. When locking the camera it gets locked on the
player keeping the zoom level. Camera can be zoomed in and out in the given
range with the mouse wheel.